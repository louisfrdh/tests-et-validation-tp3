﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TennisDomain
{
    public class Player
    {
        public PlayerScore PlayerScore = new PlayerScore();
        public bool Advantage;
        public int Id;

        public Player(int id)
        {
            Id = id;
        }

        public bool HasScore40() => PlayerScore.IsMaximumScore();

        public int Score() => PlayerScore.Score;
        public int Games() => PlayerScore.Games;

        public void WinPoint()
        {
            PlayerScore.IncreaseScore();
        }

        public void WinGame()
        {
            PlayerScore.IncreaseGames();
            Advantage = false;
            PlayerScore.ResetScore();
        }

        public void LoseGame()
        {
            Advantage = false;
            PlayerScore.ResetScore();
        }

        public void LoseAdvantage()
        {
            Advantage = false;
        }
        public void WinAdvantage()
        {
            Advantage = true;
        }
    }
}
