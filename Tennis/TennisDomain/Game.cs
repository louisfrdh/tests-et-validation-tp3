﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TennisDomain
{
    public class Game
    {
        private Player[] players = new Player[] {new Player(1), new Player(2)};

        public void PlayerScore(int playerIndex)
        {
            var player = players[playerIndex];
            if(player.HasScore40())
            {
                Player opponent = players.Where(opponent => opponent.Id != player.Id).First();
                
                if (player.Advantage)
                {
                    PlayerWinGame(player,opponent);
                }
                else if(opponent.HasScore40())
                {
                    if(opponent.Advantage)
                        OpponenentLoseAdvantage(opponent);
                    else
                        PlayerWinAdvantage(player);
                }
                else
                {
                    PlayerWinGame(player, opponent);
                }
            }
            else 
            {
                player.WinPoint();
            }
        }

        public int GetPlayerScore(int playerIndex) => players[playerIndex].Score();
        public int GetPlayerGames(int playerIndex) => players[playerIndex].Games();
        public bool PlayerHasAdvantage(int playerIndex) => players[playerIndex].Advantage;

        private void PlayerWinGame(Player player, Player opponent)
        {
            player.WinGame();
            opponent.LoseGame();
        }

        private void OpponenentLoseAdvantage(Player opponent)
        {
            opponent.LoseAdvantage();
        }
        private void PlayerWinAdvantage(Player player)
        {
            player.WinAdvantage();
        }
    }
}
