using TennisDomain;
using Xunit;

namespace TennisTest
{
    public class ScoreTest
    {
        private PlayerScore playerScore = new PlayerScore();
        private Game game = new Game();

        [Fact]
        public void WinPointWithScore0_SetScore15()
        {
            playerScore.IncreaseScore();
            int expectedScore = 15;
            Assert.Equal(expectedScore, playerScore.Score);
        }

        [Fact]
        public void WinPointWithScore15_SetScore30()
        {
            playerScore.IncreaseScore();
            playerScore.IncreaseScore();
            int expectedScore = 30;
            Assert.Equal(expectedScore, playerScore.Score);
        }

        [Fact]
        public void WinPointWithScore30_SetScore40()
        {
            playerScore.IncreaseScore();
            playerScore.IncreaseScore();
            playerScore.IncreaseScore();
            int expectedScore = 40;
            Assert.Equal(expectedScore, playerScore.Score);
        }

        [Fact]
        public void WinPointWithScore40_WinGame()
        {
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(0);

            int expectedGames = 1;
            Assert.Equal(expectedGames, game.GetPlayerGames(0));
        }

        [Fact]
        public void WinPointWithScore40_ResetScore()
        {
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(0);

            int expectedScore = 0;
            Assert.Equal(expectedScore, game.GetPlayerScore(0));
        }

        [Fact]
        public void WinPointWithBothScores40_WinAdvantage()
        {
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(1);
            game.PlayerScore(1);
            game.PlayerScore(1);
            game.PlayerScore(0);

            Assert.True(game.PlayerHasAdvantage(0));
        }

        [Fact]
        public void WinPointWhileBothScores40AndOpponentHasAdvantage_RemoveAdvantageFromOpponent()
        {
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(0);
            game.PlayerScore(1);
            game.PlayerScore(1);
            game.PlayerScore(1);
            game.PlayerScore(1);
            game.PlayerScore(0);

            Assert.False(game.PlayerHasAdvantage(1));
        }
    }
}