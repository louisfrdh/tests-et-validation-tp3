﻿namespace FizzBuzzDomain
{
    public class Generator
    {
        private string fizzBuzz;

        public string Generate(int n)
        {
            fizzBuzz = string.Empty;
            for (int i = 1; i <= n; i++)
            {
                fizzBuzz += GetStringFromInt(i);
                AddSpaceToFizzBuzz();
            }
            return fizzBuzz;
        }

        private string GetStringFromInt(int i)
        {
            if(NumberIsMultipleOf3And5(i))
                return "FizzBuzz";
            if (NumberIsMultipleOf3(i))
                return "Fizz";
            if (NumberIsMultipleOf5(i))
                return "Buzz";
            return i.ToString();
        }

        private bool NumberIsMultipleOf3And5(int i)
        {
            return NumberIsMultipleOf3(i) && NumberIsMultipleOf5(i);
        }

        private bool NumberIsMultipleOf3(int number)
        {
            if (number == 0)
                return false;
            return number % 3 == 0;
        }

        private bool NumberIsMultipleOf5(int number)
        {
            if (number == 0)
                return false;
            return number % 5 == 0;
        }

        private void AddSpaceToFizzBuzz()
        {
            fizzBuzz += " ";
        }
    }
}