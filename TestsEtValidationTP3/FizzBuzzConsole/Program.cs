﻿using FizzBuzzDomain;

const int MINIMUM = 15;
const int MAXIMUM = 150;

Console.WriteLine("Hello, World!");

Generator generator = new Generator();
int input;
while (true)
{
    do
    {
        Console.WriteLine("Please enter a number between 15 and 150 \n >>");
        try
        {
            input = Int32.Parse(Console.ReadLine());
        }
        catch (Exception)
        {
            input = 0;
        }
    } while (input < MINIMUM || input > MAXIMUM);

    string fizzbuzz = generator.Generate(input);
    Console.WriteLine($"Generated fizzbuzz : \n{fizzbuzz}\n");
}