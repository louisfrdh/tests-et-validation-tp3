﻿namespace BowlingDomain;
public class Bowling
{
    private int score=0;

    public void Play(int throwScore)
    {
        
        if (throwScore == 0) return;
        else score += throwScore;
    }

    public int Score()
    {
        return score;
    }
}

